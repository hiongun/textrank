import sys, time

import pykss     # 문장분리기
import tokenizer # 토크나이저
import textrank  # 주요키워드, 주요문장 추출

def DEBUG_PRINT(str_line):
#{
    import inspect
    print("(%s,%d) %s" % (__file__, inspect.stack()[1][2], str_line.strip()))
#}

if __name__ == "__main__":
#{
    if len(sys.argv) < 2:
        print("Usage: %s filename.txt" % (sys.argv[0]))
        sys.exit(0)

    fp = open(sys.argv[1], "r")

    for text in fp:
        # text = fp.read()
        text = text.replace("\n", " ")
        DEBUG_PRINT("text: %s" % (text))

        DEBUG_PRINT("start pykss.split_sentences()")
        t0 = time.time()
        import pykss
        sents = pykss.split_sentences(text)
        t1 = time.time()
        DEBUG_PRINT("finished pykss.split_sentences(): %0.1f" % (t1-t0))

        import tokenizer
        out_sents = textrank.textrank_keysentences(sents, tokenize=tokenizer.nouns_tokenize)
        # out_sents = textrank.textrank_keysentences(sents, tokenize=simple_tokenize)
        # out_sents = textrank.textrank_keysentences(sents, tokenize=tokenizer.twitter_tokenize)
        for out_sent in out_sents:
            DEBUG_PRINT("idx: %d" % (out_sent[0]))
            DEBUG_PRINT("Rank: %f" % (out_sent[1]))
            DEBUG_PRINT("sent: %s" % (out_sent[2]))

        import tokenizer
        out_words = textrank.textrank_keywords(sents, tokenize=tokenizer.morph_tokenize)
        # out_words = textrank.textrank_keywords(sents, tokenize=tokenizer.twitter_tokenize)
        # out_words = textrank.textrank_keywords(sents, tokenize=tokenizer.mecab_tokenize)
        # out_words = textrank.textrank_keywords(sents, tokenize=tokenizer.kkma_tokenize)
        # out_words = textrank.textrank_keywords(sents, tokenize=tokenizer.komoran_tokenize)
        # out_words = textrank.textrank_keywords(sents, tokenize=tokenizer.simple_tokenize)
        # out_words = textrank.textrank_keywords(sents, tokenize=tokenizer.soynlp_tokenize)
        for out_word in out_words:
            if len(out_word[0]) > 1: # 한글자 짜리 단어는 출력에서 제외한다
                DEBUG_PRINT("word: (%s, %0.1f)" % (out_word[0], out_word[1]))
        # break

    fp.close()

#}
