# textrank

**TextRank - a Pure Python Key-Words and Key-Sentences Extractor based on PageRank**


페이지랭크에 기반한 주요 키워드 추출기, 주요 문장 추출기

**Usage:**

import textrank

import pykss

text = "나는 집에 갔다. 한글 입력이 되나? 임의의 텍스트 가 올 수 있음. 길어도 됨. "

sentences = pykss.split_sentences(text)

key_sentences = textrank.textrank_keysentences(sentences, tokenizer=nouns.get_words)

  // 결과물 형태: key_sentences = [[idx, rank, sentence], [idx, rank, sentence], ...] 

key_words = textrank.textrank_keywords(sentences, tokenizer=nouns.get_words)

  // 결과물 형태: key_words = [[word, rank], [word, rank], ...]

**Required:**

https://gitlab.com/hiongun/nouns # use nouns.get_words(text) as a tokenize() function

https://gitlab.com/hiongun/pykss # pykss.split_sentences(text)

